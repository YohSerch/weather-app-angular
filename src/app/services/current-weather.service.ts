import { Injectable, isDevMode } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { Coords } from '../../structures/coords.structure';
import { Weather } from '../../structures/weather.structure';

@Injectable({
  providedIn: 'root'
})

export class CurrentWeatherService {

  public weatherSubject: Subject<any> = new Subject<any>();
  public weather$: Observable<any>;

  private endPoint: string = "https://api.openweathermap.org/data/2.5/weather";

  constructor(private http: HttpClient) {
    this.weather$ = this.weatherSubject.asObservable().pipe(
      map((data: any) => {
        let mainWeather = data.weather[0];

        let weather : Weather = {
          name: data.name,
          cod: data.cod,
          temp: data.main.temp,
          ...mainWeather
        };
        return weather;
      })
    );


    this.get({
      lat: 19.427025,
      lon: -99.167665
    });
  }

  get(coords: Coords) {
    let args = `?lat=${coords.lat}&lon=${coords.lon}&APPID=${environment.openWeatherKey}&units=metric`;
    let url = this.endPoint + args;

    // if(isDevMode) {
    //   url = "assets/weather.json"
    // }

    this.http.get(url).subscribe(this.weatherSubject);
  }
}
